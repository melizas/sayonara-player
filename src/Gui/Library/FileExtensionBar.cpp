/* LibraryFileExtensionBar.cpp */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FileExtensionBar.h"
#include "Components/Library/AbstractLibrary.h"

#include "Utils/Language/Language.h"
#include "Utils/ExtensionSet.h"
#include "Utils/Settings/Settings.h"

#include <QList>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>

namespace
{
	constexpr const auto LayoutSpacing = 10;

	void setBold(QWidget* widget)
	{
		auto font = widget->font();
		font.setBold(true);
		widget->setFont(font);
	}
}

namespace Library
{
	struct FileExtensionBar::Private
	{
		QLabel* labFilter;
		AbstractLibrary* library {nullptr};
		QLayout* btnLayout {nullptr};
		QPushButton* btnClose {nullptr};
		QMap<QString, QPushButton*> extensionButtonMap;

		explicit Private(QWidget* parent) :
			labFilter {new QLabel(parent)} {}
	};

	FileExtensionBar::FileExtensionBar(QWidget* parent) :
		Gui::Widget(parent),
		m {Pimpl::make<Private>(this)}
	{
		auto* layout = new QHBoxLayout(this);
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(LayoutSpacing);
		setLayout(layout);

		setBold(m->labFilter);
		layout->addWidget(m->labFilter);

		auto* buttonWidget = new QWidget();
		m->btnLayout = new QHBoxLayout(buttonWidget);
		m->btnLayout->setContentsMargins(0, 0, 0, 0);
		m->btnLayout->setSpacing(LayoutSpacing);
		m->btnLayout->setSizeConstraint(QLayout::SetMinimumSize);

		buttonWidget->setLayout(m->btnLayout);
		layout->addWidget(buttonWidget);

		auto* spacer = new QSpacerItem(LayoutSpacing, LayoutSpacing, QSizePolicy::MinimumExpanding,
		                               QSizePolicy::Maximum);
		layout->addSpacerItem(spacer);

		m->btnClose = new QPushButton(this);
		m->btnClose->setFocusPolicy(Qt::NoFocus);
		layout->addWidget(m->btnClose);
		connect(m->btnClose, &QPushButton::clicked, this, &FileExtensionBar::closeClicked);
	}

	FileExtensionBar::~FileExtensionBar()
	{
		for(auto* extensionButton: qAsConst(m->extensionButtonMap))
		{
			m->btnLayout->removeWidget(extensionButton);
			extensionButton->setParent(nullptr);
			extensionButton->deleteLater();
		}
	}

	void FileExtensionBar::init(AbstractLibrary* library)
	{
		m->library = library;
	}

	void FileExtensionBar::refresh()
	{
		const auto extensions = m->library->extensions();
		const auto extensionStrings = extensions.extensions();

		clear();

		const auto hasMultipleExtensions = (extensionStrings.size() > 1);
		if(!hasMultipleExtensions)
		{
			return;
		}

		for(const auto& extension: extensionStrings)
		{
			if(m->extensionButtonMap.contains(extension))
			{
				auto* extensionButton = m->extensionButtonMap[extension];
				extensionButton->setVisible(true);
			}

			else
			{
				auto* extensionButton = new QPushButton();
				extensionButton->setFocusPolicy(Qt::NoFocus);
				extensionButton->setText(extension);
				extensionButton->setCheckable(true);
				extensionButton->setChecked(extensions.isEnabled(extension));
				extensionButton->setVisible(true);

				connect(extensionButton, &QPushButton::toggled, this, &FileExtensionBar::buttonToggled);

				m->btnLayout->addWidget(extensionButton);
				m->extensionButtonMap[extension] = extensionButton;
			}
		}
	}

	void FileExtensionBar::clear()
	{
		for(auto* extensionButton: qAsConst(m->extensionButtonMap))
		{
			extensionButton->setVisible(false);
		}
	}

	bool FileExtensionBar::hasExtensions() const
	{
		return (m->library->extensions().extensions().size() > 1);
	}

	void FileExtensionBar::buttonToggled(bool b)
	{
		auto* extensionButton = dynamic_cast<QPushButton*>(sender());

		auto extensions = m->library->extensions();
		extensions.setEnabled(extensionButton->text(), b);

		m->library->setExtensions(extensions);
	}

	void FileExtensionBar::closeClicked() // NOLINT(*-convert-member-functions-to-static)
	{
		SetSetting(Set::Lib_ShowFilterExtBar, false);
	}

	void FileExtensionBar::languageChanged()
	{
		m->btnClose->setText(Lang::get(Lang::Hide));
		m->labFilter->setText(Lang::get(Lang::Filetype));
	}
}