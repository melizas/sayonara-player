/* LyricServerMap.h, (Created on 15.05.2024) */

/* Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SAYONARA_PLAYER_LYRICSERVERENTRY_H
#define SAYONARA_PLAYER_LYRICSERVERENTRY_H

#include "Utils/Settings/SettingConvertible.h"

#include <QList>
#include <QMap>
#include <QString>

namespace Lyrics
{
	struct ServerEntry :
		public SettingConvertible
	{
		QString name;
		bool isChecked {true};

		ServerEntry();
		ServerEntry(const ServerEntry& other);
		ServerEntry(QString name, bool isChecked);
		ServerEntry& operator=(const ServerEntry& other);

		bool operator==(const ServerEntry& other) const;

		[[nodiscard]] QString toString() const override;
		bool loadFromString(const QString& str) override;
	};
}
#endif //SAYONARA_PLAYER_LYRICSERVERENTRY_H
